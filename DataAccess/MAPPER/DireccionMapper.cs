﻿using DataAccess.DAO;
using EntitiesPOJO;
using System.Collections.Generic;

namespace DataAccess.MAPPER
{
    class DireccionMapper: EntityMapper, ISqlStatement, IObjectMapper
    {
        private const string DB_COL_PROVINCIA = "PROVINCIA";
        private const string DB_COL_CANTON = "CANTON";
        private const string DB_COL_DISTRITO = "DISTRITO";


        public SqlOperation GetCreateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "CREAR_DIRECCION_PR" };

            var d = (Direccion)entity;
            operation.AddVarcharParam(DB_COL_PROVINCIA, d.provincia);
            operation.AddVarcharParam(DB_COL_CANTON, d.canton);
            operation.AddVarcharParam(DB_COL_DISTRITO, d.distrito);
            return operation;
        }


        public SqlOperation GetRetriveStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_DIRECCION_PORPROVINCIA_PR" };

            var d = (Direccion)entity;
            operation.AddVarcharParam(DB_COL_PROVINCIA, d.provincia);

            return operation;
        }

        public SqlOperation GetRetriveAllStatement()
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_TODO_DIRECCIONES_PR" };
            return operation;
        }

        public SqlOperation GetUpdateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "ACTUALIZAR_DIRECCION_PR" };

            var d = (Direccion)entity;
            operation.AddVarcharParam(DB_COL_PROVINCIA, d.provincia);
            operation.AddVarcharParam(DB_COL_CANTON, d.canton);
            operation.AddVarcharParam(DB_COL_DISTRITO, d.distrito);

            return operation;
        }

        public SqlOperation GetDeleteStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "BORRAR_DIRECCION_PR" };

            var d = (Direccion)entity;
            operation.AddVarcharParam(DB_COL_PROVINCIA, d.provincia);
            operation.AddVarcharParam(DB_COL_CANTON, d.canton);
            operation.AddVarcharParam(DB_COL_DISTRITO, d.distrito);
            return operation;
        }

        public List<BaseEntity> BuildObjects(List<Dictionary<string, object>> lstRows)
        {
            var lstResults = new List<BaseEntity>();

            foreach (var row in lstRows)
            {
                var direccion = BuildObject(row);
                lstResults.Add(direccion);
            }

            return lstResults;
        }

        public BaseEntity BuildObject(Dictionary<string, object> row)
        {
            var direccion = new Direccion
            {
                provincia = GetStringValue(row, DB_COL_PROVINCIA),
                canton = GetStringValue(row, DB_COL_CANTON),
                distrito = GetStringValue(row, DB_COL_DISTRITO)

            };

            return direccion;
        }
    }
}
