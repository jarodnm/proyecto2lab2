﻿using DataAccess.DAO;
using EntitiesPOJO;
using System.Collections.Generic;

namespace DataAccess.MAPPER
{
    class CuentaMapper : EntityMapper, ISqlStatement, IObjectMapper
    {
        private const string DB_COL_NOMBRE = "NOMBRE";
        private const string DB_COL_MONEDA = "MONEDA";
        private const string DB_COL_SALDO = "SALDO";
     

        public SqlOperation GetCreateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "CREAR_CUENTA_PR" };

            var c = (Cuenta)entity;
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            operation.AddVarcharParam(DB_COL_MONEDA, c.moneda);
            operation.AddDoubleParam(DB_COL_SALDO, c.saldo);
            return operation;
        }


        public SqlOperation GetRetriveStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_NOMBRE_CUENTA_PR" };

            var c = (Cliente)entity;
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);

            return operation;
        }

        public SqlOperation GetRetriveAllStatement()
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_TODO_CUENTAS_PR" };
            return operation;
        }

        public SqlOperation GetUpdateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "ACTUALIZAR_CUENTA_PR" };

            var c = (Cuenta)entity;
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            operation.AddVarcharParam(DB_COL_MONEDA, c.moneda);
            operation.AddDoubleParam(DB_COL_SALDO, c.saldo);

            return operation;
        }

        public SqlOperation GetDeleteStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "BORRAR_CUENTA_PR" };

            var c = (Cuenta)entity;
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            return operation;
        }

        public List<BaseEntity> BuildObjects(List<Dictionary<string, object>> lstRows)
        {
            var lstResults = new List<BaseEntity>();

            foreach (var row in lstRows)
            {
                var cuenta = BuildObject(row);
                lstResults.Add(cuenta);
            }

            return lstResults;
        }

        public BaseEntity BuildObject(Dictionary<string, object> row)
        {
            var cuenta = new Cuenta
            {
                nombre = GetStringValue(row, DB_COL_NOMBRE),
                moneda = GetStringValue(row, DB_COL_MONEDA),
                saldo = GetDoubleValue(row, DB_COL_SALDO)

            };

            return cuenta;
        }
    }
}
