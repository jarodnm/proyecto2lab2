﻿using EntitiesPOJO;
using System.Collections.Generic;


namespace DataAccess.MAPPER
{
    interface IObjectMapper
    {
        List<BaseEntity> BuildObjects(List<Dictionary<string, object>> lstRows);
        BaseEntity BuildObject(Dictionary<string, object> row);
    }
}
