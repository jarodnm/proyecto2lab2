﻿using DataAccess.DAO;
using EntitiesPOJO;
using System.Collections.Generic;

namespace DataAccess.MAPPER
{
    class CreditoMapper : EntityMapper, ISqlStatement, IObjectMapper
    {
        private const string DB_COL_MONTO = "MONTO";
        private const string DB_COL_TASA = "TASA";
        private const string DB_COL_NOMBRE = "NOMBRE";
        private const string DB_COL_CUOTA = "CUOTA";
        private const string DB_COL_FECHA_INICIO = "FECHA_INICIO";
        private const string DB_COL_ESTADO = "ESTADO";
        private const string DB_COL_SALDO_OPERACION = "SALDO_OPERACION";

        public SqlOperation GetCreateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "CREAR_CREDITO_PR" };

            var c = (Credito)entity;
            operation.AddDoubleParam(DB_COL_MONTO, c.monto);
            operation.AddDoubleParam(DB_COL_TASA, c.tasa);
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            operation.AddDoubleParam(DB_COL_CUOTA, c.cuota);
            operation.AddDateParam(DB_COL_FECHA_INICIO, c.fechaInicio);
            operation.AddVarcharParam(DB_COL_ESTADO, c.estado);
            operation.AddDoubleParam(DB_COL_SALDO_OPERACION, c.saldoOperacion);
            return operation;
        }


        public SqlOperation GetRetriveStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_NOMBRE_CREDITO_PR" };

            var c = (Credito)entity;
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);

            return operation;
        }

        public SqlOperation GetRetriveAllStatement()
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_TODO_CREDITOS_PR" };
            return operation;
        }

        public SqlOperation GetUpdateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "ACTUALIZAR_CREDITO_PR" };

            var c = (Credito)entity;
            operation.AddDoubleParam(DB_COL_MONTO, c.monto);
            operation.AddDoubleParam(DB_COL_TASA, c.tasa);
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            operation.AddDoubleParam(DB_COL_CUOTA, c.cuota);
            operation.AddDateParam(DB_COL_FECHA_INICIO, c.fechaInicio);
            operation.AddVarcharParam(DB_COL_ESTADO, c.estado);
            operation.AddDoubleParam(DB_COL_SALDO_OPERACION, c.saldoOperacion);

            return operation;
        }

        public SqlOperation GetDeleteStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "BORRAR_CREDITO_PR" };

            var c = (Credito)entity;
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            return operation;
        }

        public List<BaseEntity> BuildObjects(List<Dictionary<string, object>> lstRows)
        {
            var lstResults = new List<BaseEntity>();

            foreach (var row in lstRows)
            {
                var creditos = BuildObject(row);
                lstResults.Add(creditos);
            }

            return lstResults;
        }

        public BaseEntity BuildObject(Dictionary<string, object> row)
        {
            var credito = new Credito
            {
                monto = GetDoubleValue(row, DB_COL_MONTO),
                tasa = GetDoubleValue(row, DB_COL_TASA),
                nombre = GetStringValue(row, DB_COL_NOMBRE),
                cuota = GetDoubleValue(row, DB_COL_CUOTA),
                fechaInicio = GetDateValue(row, DB_COL_FECHA_INICIO),
                estado = GetStringValue(row, DB_COL_ESTADO),
                saldoOperacion = GetDoubleValue(row, DB_COL_SALDO_OPERACION)

            };

            return credito;
        }

    }
}

