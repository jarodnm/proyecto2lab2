﻿using DataAccess.DAO;
using EntitiesPOJO;
using System.Collections.Generic;

namespace DataAccess.MAPPER
{
    public class ClienteMapper : EntityMapper, ISqlStatement, IObjectMapper
    {
        private const string DB_COL_CEDULA = "CEDULA";
        private const string DB_COL_NOMBRE = "NOMBRE";
        private const string DB_COL_APELLIDO = "APELLIDO";
        private const string DB_COL_FECHA_NACIMIENTO = "FECHA_NACIMIENTO" ;
        private const string DB_COL_EDAD = "EDAD";
        private const string DB_COL_ESTADO_CIVIL = "ESTADO_CIVIL";
        private const string DB_COL_GENERO = "GENERO";

        public SqlOperation GetCreateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "CREAR_CLIENTE_PR" };

            var c = (Cliente)entity;
            operation.AddVarcharParam(DB_COL_CEDULA, c.cedula);
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            operation.AddVarcharParam(DB_COL_APELLIDO, c.apellidos);
            operation.AddDateParam(DB_COL_FECHA_NACIMIENTO, c.fechaNacimiento);
            operation.AddIntParam(DB_COL_EDAD, c.edad);
            operation.AddVarcharParam(DB_COL_ESTADO_CIVIL, c.estadoCivil);
            operation.AddVarcharParam(DB_COL_GENERO, c.genero);
            return operation;
        }


        public SqlOperation GetRetriveStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_ID_CLIENTE_PR" };

            var c = (Cliente)entity;
            operation.AddVarcharParam(DB_COL_CEDULA, c.cedula);

            return operation;
        }

        public SqlOperation GetRetriveAllStatement()
        {
            var operation = new SqlOperation { ProcedureName = "LISTAR_TODO_CLIENTES_PR" };
            return operation;
        }

        public SqlOperation GetUpdateStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "ACTUALIZAR_CLIENTE_PR" };

            var c = (Cliente)entity;
            operation.AddVarcharParam(DB_COL_CEDULA, c.cedula);
            operation.AddVarcharParam(DB_COL_NOMBRE, c.nombre);
            operation.AddVarcharParam(DB_COL_APELLIDO, c.apellidos);
            operation.AddDateParam(DB_COL_FECHA_NACIMIENTO, c.fechaNacimiento);
            operation.AddIntParam(DB_COL_EDAD, c.edad);
            operation.AddVarcharParam(DB_COL_ESTADO_CIVIL, c.estadoCivil);
            operation.AddVarcharParam(DB_COL_GENERO, c.genero);

            return operation;
        }

        public SqlOperation GetDeleteStatement(BaseEntity entity)
        {
            var operation = new SqlOperation { ProcedureName = "BORRAR_CLIENTE_PR" };

            var c = (Cliente)entity;
            operation.AddVarcharParam(DB_COL_CEDULA, c.cedula);
            return operation;
        }

        public List<BaseEntity> BuildObjects(List<Dictionary<string, object>> lstRows)
        {
            var lstResults = new List<BaseEntity>();

            foreach (var row in lstRows)
            {
                var cliente = BuildObject(row);
                lstResults.Add(cliente);
            }

            return lstResults;
        }

        public BaseEntity BuildObject(Dictionary<string, object> row)
        {
            var cliente = new Cliente
            {
                cedula = GetStringValue(row, DB_COL_CEDULA),
                nombre = GetStringValue(row, DB_COL_NOMBRE),
                apellidos = GetStringValue(row, DB_COL_APELLIDO),
                fechaNacimiento = GetDateValue(row, DB_COL_FECHA_NACIMIENTO),
                edad= GetIntValue(row, DB_COL_EDAD),
                estadoCivil = GetStringValue(row,DB_COL_ESTADO_CIVIL),
                genero = GetStringValue(row,DB_COL_GENERO)

            };

            return cliente;
        }

    }
}

