﻿Imports DataAccess.CRUD

Public Class DireccionManagement

    Private crudDireccion As DireccionCrudFactory

    Public Sub DireccionManagement()
        crudDireccion = New DireccionCrudFactory()
    End Sub

    Public Sub Create(direccion As EntitiesPOJO.Direccion)
        crudDireccion.Create(direccion)
    End Sub

    Public Function RetrieveAll() As List(Of EntitiesPOJO.Direccion)
        Dim lista As List(Of EntitiesPOJO.Direccion) = crudDireccion.RetrieveAll(Of EntitiesPOJO.Direccion)
        Return lista
    End Function

    Public Function RetrieveById(direccion As EntitiesPOJO.Direccion) As EntitiesPOJO.Direccion
        Dim direccionEnc As EntitiesPOJO.Direccion = crudDireccion.Retrieve(Of EntitiesPOJO.Direccion)(direccion)
        Return direccionEnc
    End Function
    Friend Sub Update(direccion As EntitiesPOJO.Direccion)
        crudDireccion.Update(direccion)
    End Sub
    Friend Sub Delete(direccion As EntitiesPOJO.Direccion)
        crudDireccion.Delete(direccion)
    End Sub
End Class
