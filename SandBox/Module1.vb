﻿Imports EntitiesPOJO

Module Module1
    Sub Main()
        Menu()
    End Sub

    Sub Menu()
        Try
            Dim mngCliente As New ClienteManagement
            mngCliente.ClienteManagement()
            Dim cliente = New EntitiesPOJO.Cliente()

            Dim mngCuenta As New CuentaManagement
            mngCuenta.CuentaManagement()
            Dim cuenta = New EntitiesPOJO.Cuenta()

            Dim mngCredito As New CreditoManagement
            mngCredito.CreditoManagement()
            Dim credito = New EntitiesPOJO.Credito()

            Dim mngDireccion As New DireccionManagement
            mngDireccion.DireccionManagement()
            Dim Direccion = New EntitiesPOJO.Direccion()

            Console.WriteLine("*******MENU PRINCIPAL*******")
            Console.WriteLine("****CLIENTES****")
            Console.WriteLine("1.Registrar")
            Console.WriteLine("2.Listar")
            Console.WriteLine("3.Buscar")
            Console.WriteLine("4.Actualizar")
            Console.WriteLine("5.Eliminar")
            Console.WriteLine("****CUENTAS****")
            Console.WriteLine("6.Registrar")
            Console.WriteLine("7.Listar")
            Console.WriteLine("8.Buscar")
            Console.WriteLine("9.Actualizar")
            Console.WriteLine("10.Eliminar")
            Console.WriteLine("****CREDITOS****")
            Console.WriteLine("11.Registrar")
            Console.WriteLine("12.Listar")
            Console.WriteLine("13.Buscar")
            Console.WriteLine("14.Actualizar")
            Console.WriteLine("15.Eliminar")
            Console.WriteLine("****DIRECCIONES****")
            Console.WriteLine("16.Registrar")
            Console.WriteLine("17.Listar")
            Console.WriteLine("18.Buscar")
            Console.WriteLine("19.Actualizar")
            Console.WriteLine("20.Eliminar")
            Console.WriteLine("Porfavor digite el numero de la accion que desea ejecutar numero")
            Console.WriteLine("******************")
            Dim opc As Integer = Console.ReadLine()

            Select Case opc
                Case 1
                    Console.WriteLine("*******")
                    Console.WriteLine("*REGISTRAR CLIENTE*")
                    Console.WriteLine("*******")
                    Console.WriteLine("Digite la Cedula, Nombre, Apellido, Fecha de Nacimiento,Estado civil y genero")
                    Console.WriteLine("Separados por una coma [ , ]")
                    Dim info = Console.ReadLine()
                    Dim infoArray() As String = Split(info, ",")

                    cliente = New EntitiesPOJO.Cliente(infoArray)
                    mngCliente.Create(cliente)
                    Console.WriteLine("El cliente se ha creado correctamente")
                Case 2
                    Console.WriteLine("*******")
                    Console.WriteLine("**LISTAR CLIENTES**")
                    Console.WriteLine("*******")
                    Dim lstCliente = mngCliente.RetrieveAll()
                    Dim count = 0
                    For Each c In lstCliente
                        count += 1
                        Console.WriteLine(count & " ==> " & c.GetEntityInformation())

                    Next
                Case 3
                    Console.WriteLine("*******")
                    Console.WriteLine("LISTAR POR ID**")
                    Console.WriteLine("*******")
                    cliente.Cedula = Console.ReadLine()
                    cliente = mngCliente.RetrieveById(cliente)
                    If cliente IsNot Nothing Then
                        Console.WriteLine("El cliente es: " & cliente.GetEntityInformation())

                    End If
                Case 4
                    Console.WriteLine("******")
                    Console.WriteLine("**ACTUALIZAR**")
                    Console.WriteLine("******")
                    Console.WriteLine("Digite el id del cliente que desea actualizar:")
                    cliente.Cedula = Console.ReadLine()
                    cliente = mngCliente.RetrieveById(cliente)
                    If cliente IsNot Nothing Then
                        Console.WriteLine("Cliente: " & cliente.GetEntityInformation())
                        Console.WriteLine("Digite el nombre: " & cliente.Nombre)
                        cliente.Nombre = Console.ReadLine()
                        Console.WriteLine("Digite el apellido: " & cliente.apellidos)
                        cliente.apellidos = Console.ReadLine()
                        Console.WriteLine("Digite la edad: " & cliente.EstadoCivil)
                        cliente.EstadoCivil = Console.ReadLine()
                        Console.WriteLine("Digite el estado civil: " & cliente.EstadoCivil)
                        cliente.EstadoCivil = Console.ReadLine()
                        mngCliente.Update(cliente)
                        Console.WriteLine("El cliente fue actualizado correctamente")
                    Else
                        Throw New Exception("El cliente no esta registrado, por favor verifique los datos")
                    End If

                Case 5
                    Console.WriteLine("********")
                    Console.WriteLine("**ELIMINAR CLIENTE**")
                    Console.WriteLine("********")
                    Console.WriteLine("Digite el id del cliente que desea eliminar:")
                    cliente.Cedula = Console.ReadLine()
                    cliente = mngCliente.RetrieveById(cliente)
                    If cliente IsNot Nothing Then
                        Console.WriteLine("Cliente: " & cliente.GetEntityInformation())
                        Console.WriteLine("Esta seguro que desea eliminar el cliente? Y/N")
                        Dim delete = Console.ReadLine()
                        If delete.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then
                            mngCliente.Delete(cliente)
                            Console.WriteLine("El cliente ha sido eliminado correctamente")

                        End If

                    Else
                        Throw New Exception("El cliente no esta registrado")

                    End If

                Case 6
                    Console.WriteLine("********")
                    Console.WriteLine("**REGISTRAR CUENTA**")
                    Console.WriteLine("********")
                    Console.WriteLine("Digite el Nombre, la moneda y el saldo")
                    Console.WriteLine("Separados por una coma [ , ]")
                    Dim info = Console.ReadLine()
                    Dim infoArray() As String = Split(info, ",")

                    cuenta = New EntitiesPOJO.Cuenta(infoArray)
                    mngCuenta.Create(cuenta)
                    Console.WriteLine("La cuenta se ha sido creada correctamente")
                Case 7
                    Console.WriteLine("********")
                    Console.WriteLine("**LISTAR CUENTAS**")
                    Console.WriteLine("********")
                    Dim lstCuenta = mngCuenta.RetrieveAll()
                    Dim count = 0
                    For Each c In lstCuenta
                        count += 1
                        Console.WriteLine(count & "Cuenta: " & c.GetEntityInformation())

                    Next
                Case 8
                    Console.WriteLine("*******")
                    Console.WriteLine("**LISTAR POR ID**")
                    Console.WriteLine("*******")
                    cuenta.Nombre = Console.ReadLine()
                    cuenta = mngCuenta.RetrieveById(cuenta)
                    If cuenta IsNot Nothing Then
                        Console.WriteLine("Cuenta: " & cuenta.GetEntityInformation())

                    End If
                Case 9
                    Console.WriteLine("*********")
                    Console.WriteLine("**ACTUALIZAR CUENTA**")
                    Console.WriteLine("*********")
                    Console.WriteLine("Digite el id de la cuenta que desea actualizar:")

                Case 10
                    Console.WriteLine("*******")
                    Console.WriteLine("**ELIMINAR CUENTA**")
                    Console.WriteLine("*******")
                    Console.WriteLine("Digite el id de la cuenta que desea eliminar:")
                    cuenta.Nombre = Console.ReadLine()
                    cuenta = mngCuenta.RetrieveById(cuenta)
                    If cuenta IsNot Nothing Then
                        Console.WriteLine("Cuenta: " & cliente.GetEntityInformation())
                        Console.WriteLine("Desea eliminar la cuenta? Y/N")
                        Dim delete = Console.ReadLine()
                        If delete.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then
                            mngCuenta.Delete(cuenta)
                            Console.WriteLine("La cuenta ha sido eliminada correctamente")

                        End If

                    Else
                        Throw New Exception("La cuenta no esta registrada, por favor verifique los datos")

                    End If
                Case 11
                    Console.WriteLine("*********")
                    Console.WriteLine("**REGISTRAR CREDITO**")
                    Console.WriteLine("*********")
                    Console.WriteLine("Digite el id,monto, tasa, nombre, cuota, fecha inicio, estado, saldo operacion")
                    Console.WriteLine("Separados por una coma [ , ]")
                    Dim info = Console.ReadLine()
                    Dim infoArray() As String = Split(info, ",")

                    credito = New Credito(infoArray)
                    mngCredito.Create(credito)
                    Console.WriteLine("Se ha creado el credito correctamente")
                Case 12
                    Console.WriteLine("*******")
                    Console.WriteLine("**LISTAR CREDITOS**")
                    Console.WriteLine("*******")
                    Dim lstCreditos = mngCredito.RetrieveAll()
                    Dim count = 0
                    For Each c In lstCreditos
                        count += 1
                        Console.WriteLine(count & "Credito: " & c.GetEntityInformation())

                    Next
                Case 13
                    Console.WriteLine("*******")
                    Console.WriteLine("**LISTAR POR ID**")
                    Console.WriteLine("*******")
                    credito.Nombre = Console.ReadLine()
                    credito = mngCredito.RetrieveById(credito)
                    If credito IsNot Nothing Then
                        Console.WriteLine("Credito: " & credito.GetEntityInformation())

                    End If
                Case 14
                    Console.WriteLine("********")
                    Console.WriteLine("**ACTUALIZAR CREDITO**")
                    Console.WriteLine("********")
                    Console.WriteLine("Digite el id del credito que desea actualizar:")

                Case 15
                    Console.WriteLine("*******")
                    Console.WriteLine("**ELIMINAR CREDITO*")
                    Console.WriteLine("*******")
                    Console.WriteLine("Digite el id del credito que desea eliminar:")
                    credito.Nombre = Console.ReadLine()
                    credito = mngCredito.RetrieveById(credito)
                    If credito IsNot Nothing Then
                        Console.WriteLine("Credito: " & credito.GetEntityInformation())
                        Console.WriteLine("Desea eliminar el credito? Y/N")
                        Dim delete = Console.ReadLine()
                        If delete.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then
                            mngCredito.Delete(credito)
                            Console.WriteLine("Credito ha sido eliminado correctamente")

                        End If

                    Else
                        Throw New Exception("El credito no esta registrado, verifique los que los datos sean correctos")

                    End If
                Case 16
                    Console.WriteLine("*********")
                    Console.WriteLine("*REGISTRAR DIRECCION*")
                    Console.WriteLine("*********")
                    Console.WriteLine("Digite la provincia, canton, distrito")
                    Console.WriteLine("Separados por una coma [ , ]")
                    Dim info = Console.ReadLine()
                    Dim infoArray() As String = Split(info, ",")

                    Direccion = New Direccion(infoArray)
                    mngDireccion.Create(Direccion)
                    Console.WriteLine("La direccion se ha creado correctamente")
                Case 17
                    Console.WriteLine("********")
                    Console.WriteLine("**LISTAR DIRECCION**")
                    Console.WriteLine("********")
                    Dim lstDireccion = mngDireccion.RetrieveAll()
                    Dim count = 0
                    For Each c In lstDireccion
                        count += 1
                        Console.WriteLine(count & "Direccion: " & c.GetEntityInformation())

                    Next
                Case 18
                    Console.WriteLine("*******")
                    Console.WriteLine("LISTAR POR ID**")
                    Console.WriteLine("*******")
                    Direccion.Provincia = Console.ReadLine()
                    Direccion = mngDireccion.RetrieveById(Direccion)
                    If Direccion IsNot Nothing Then
                        Console.WriteLine("Direccion: " & Direccion.GetEntityInformation())

                    End If
                Case 19
                    Console.WriteLine("**********")
                    Console.WriteLine("**ACTUALIZAR DIRECCION**")
                    Console.WriteLine("**********")
                    Console.WriteLine("Digite la provincia que desea modificar:")
                    Direccion.Provincia = Console.ReadLine()
                    Direccion = mngDireccion.RetrieveById(Direccion)
                    If Direccion IsNot Nothing Then
                        Console.WriteLine("Direccion: " & Direccion.GetEntityInformation())
                        Console.WriteLine("Digite el canton: " & Direccion.Canton
                                          )
                        Direccion.Canton = Console.ReadLine()
                        Console.WriteLine("Digite el distrito: " & Direccion.Distrito
                                          )
                        Direccion.Distrito = Console.ReadLine()
                        mngDireccion.Update(Direccion)
                        Console.WriteLine("La direccion fue actualizada")
                    Else
                        Throw New Exception("La direccion no esta registrada, por favor verifique sean los datos correctos")
                    End If




                Case 20
                    Console.WriteLine("********")
                    Console.WriteLine("**ELIMINAR DIRECCION**")
                    Console.WriteLine("********")
                    Console.WriteLine("Digite la provincia que desea eliminar:")
                    Direccion.Provincia = Console.ReadLine()
                    Direccion = mngDireccion.RetrieveById(Direccion)
                    If Direccion IsNot Nothing Then
                        Console.WriteLine("Direccion: " & Direccion.GetEntityInformation())
                        Console.WriteLine("Desea eliminar la direccion? Y/N")
                        Dim delete = Console.ReadLine()
                        If delete.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then
                            mngDireccion.Delete(Direccion)
                            Console.WriteLine("La direccion ha sido eliminada correctamente")

                        End If

                    Else
                        Throw New Exception("La direccion no esta registrada, por favor verifique sean los datos correctos")

                    End If


            End Select

        Catch ex As Exception
            Console.WriteLine("***")
            Console.WriteLine("ERROR: " & ex.Message)
            Console.WriteLine(ex.StackTrace)
            Console.WriteLine("***")
        Finally
            Console.WriteLine("Desea continuar? Y/N")
            Dim moreActions As String = Console.ReadLine()
            If moreActions.Equals("Y", StringComparison.CurrentCultureIgnoreCase) Then Menu()
        End Try
    End Sub
End Module
