﻿Imports DataAccess.CRUD

Public Class ClienteManagement

    Private crudCliente As ClienteCrudFactory

    Public Sub ClienteManagement()
        crudCliente = New ClienteCrudFactory()
    End Sub

    Public Sub Create(cliente As EntitiesPOJO.Cliente)
        crudCliente.Create(cliente)
    End Sub

    Public Function RetrieveAll() As List(Of EntitiesPOJO.Cliente)
        Dim lista As List(Of EntitiesPOJO.Cliente) = crudCliente.RetrieveAll(Of EntitiesPOJO.Cliente)
        Return lista
    End Function

    Public Function RetrieveById(cliente As EntitiesPOJO.Cliente) As EntitiesPOJO.Cliente
        Dim clienteEnc As EntitiesPOJO.Cliente = crudCliente.Retrieve(Of EntitiesPOJO.Cliente)(cliente)
        Return clienteEnc
    End Function
    Friend Sub Update(cliente As EntitiesPOJO.Cliente)
        crudCliente.Update(cliente)
    End Sub
    Friend Sub Delete(cliente As EntitiesPOJO.Cliente)
        crudCliente.Delete(cliente)
    End Sub
End Class

