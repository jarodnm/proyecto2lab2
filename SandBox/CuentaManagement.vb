﻿Imports DataAccess.CRUD
Public Class CuentaManagement

    Private crudCuenta As CuentaCrudFactory

    Public Sub CuentaManagement()
        crudCuenta = New CuentaCrudFactory()
    End Sub

    Public Sub Create(cuenta As EntitiesPOJO.Cuenta)
        crudCuenta.Create(cuenta)
    End Sub

    Public Function RetrieveAll() As List(Of EntitiesPOJO.Cuenta)
        Dim lista As List(Of EntitiesPOJO.Cuenta) = crudCuenta.RetrieveAll(Of EntitiesPOJO.Cuenta)
        Return lista
    End Function

    Public Function RetrieveById(cuenta As EntitiesPOJO.Cuenta) As EntitiesPOJO.Cuenta
        Dim cuentaEnc As EntitiesPOJO.Cuenta = crudCuenta.Retrieve(Of EntitiesPOJO.Cuenta)(cuenta)
        Return cuentaEnc
    End Function
    Friend Sub Update(cuenta As EntitiesPOJO.Cuenta)
        crudCuenta.Update(cuenta)
    End Sub
    Friend Sub Delete(cuenta As EntitiesPOJO.Cuenta)
        crudCuenta.Delete(cuenta)
    End Sub
End Class
