﻿Imports DataAccess.CRUD

Public Class CreditoManagement

    Private crudCredito As CreditoCrudFactory

    Public Sub CreditoManagement()
        crudCredito = New CreditoCrudFactory()
    End Sub

    Public Sub Create(credito As EntitiesPOJO.Credito)
        crudCredito.Create(credito)
    End Sub

    Public Function RetrieveAll() As List(Of EntitiesPOJO.Credito)
        Dim lista As List(Of EntitiesPOJO.Credito) = crudCredito.RetrieveAll(Of EntitiesPOJO.Credito)
        Return lista
    End Function

    Public Function RetrieveById(credito As EntitiesPOJO.Credito) As EntitiesPOJO.Credito
        Dim creditoEnc As EntitiesPOJO.Credito = crudCredito.Retrieve(Of EntitiesPOJO.Credito)(credito)
        Return creditoEnc
    End Function
    Friend Sub Update(credito As EntitiesPOJO.Credito)
        crudCredito.Update(credito)
    End Sub
    Friend Sub Delete(credito As EntitiesPOJO.Credito)
        crudCredito.Delete(credito)
    End Sub
End Class
