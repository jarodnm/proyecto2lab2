﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesPOJO
{
    public class Credito:BaseEntity
    {
        public double monto { get; set; }
        public double tasa { get; set; }
        public String nombre { get; set; }
        public double cuota { get; set; }
        public DateTime fechaInicio { get; set; }
        public String estado { get; set; }
        public double saldoOperacion { get; set; }

        public Credito()
        {

        }
        public Credito(string[] infoArray)
        {
            if (infoArray != null && infoArray.Length >= 7)
            {
                monto = double.Parse(infoArray[0]);
                tasa = double.Parse(infoArray[1]);
                nombre = infoArray[2];
                cuota = double.Parse(infoArray[3]);
                fechaInicio = DateTime.ParseExact(infoArray[4], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                estado = infoArray[5];
                saldoOperacion = double.Parse(infoArray[6]);
             
            }
            else
            {
                throw new Exception("Revise que digito todo correctamente");
            }

        }

    }
}
