﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesPOJO
{
    public class Cliente : BaseEntity
    {
        public String cedula { get; set; }
        public String nombre { get; set; }
        public String apellidos { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public int edad { get; set; }
        public String estadoCivil { get; set; }
        public String genero { get; set; }

        public Cliente()
        {

        }

        public int generarEdad(DateTime fechaNacimiento)
        {
            int edad = DateTime.Today.AddTicks(-fechaNacimiento.Ticks).Year - 1;
            return edad;
        }

        public Cliente(string[] infoArray)
        {
            if (infoArray != null && infoArray.Length >= 6)
            {
                cedula = infoArray[0];
                nombre = infoArray[1];
                apellidos = infoArray[2];
                fechaNacimiento = DateTime.ParseExact(infoArray[3], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                edad = generarEdad(fechaNacimiento);
                estadoCivil = infoArray[4];
                genero = infoArray[5];

            }
            else
            {
                throw new Exception("Revise que digito todo correctamente");
            }

        }
    }
}
