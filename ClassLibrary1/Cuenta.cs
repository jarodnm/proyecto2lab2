﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesPOJO
{
    public class Cuenta:BaseEntity
    {
        public String nombre { get; set; }
        public String moneda { get; set; }
        public double saldo { get; set; }

        public Cuenta(){


        }

        public Cuenta(string[] infoArray)
        {
            if (infoArray != null && infoArray.Length >= 3)
            {
                nombre = infoArray[0];
                moneda = infoArray[1];
                saldo = double.Parse(infoArray[2]);
            }
            else
            {
                throw new Exception("Revise que digito todo correctamente");
            }

        }


    }
}
